package com.aula.itau.seguranca.auth;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JWTUtil {

    private static final String SEGREDO = "ndnjedjenuj3344ndj21";

    private static final Long TEMPO_DE_VALIDADE = 86000L;

    //Metodo para gerar o token
    public String gerarToken(String email){
        Date dataVencimento = new Date(System.currentTimeMillis()+TEMPO_DE_VALIDADE);

        String token = Jwts.builder().setSubject(email)
                .setExpiration(dataVencimento)
                .signWith(SignatureAlgorithm.HS512,SEGREDO.getBytes()).compact();

        return token;
    }

    public String getEmail(String token){
        Claims claims = getClaims(token);
        String email = claims.getSubject();
        return email;
    }

    private Claims getClaims(String token) {
       return Jwts.parser().setSigningKey(SEGREDO.getBytes()).parseClaimsJws(token).getBody();
    }

    public boolean tokenValido(String token){
        Claims claims = getClaims(token);
        String email = claims.getSubject();

        Date dataExpiracao = claims.getExpiration();
        Date dataAtual = new Date(System.currentTimeMillis());

        if(email != null && dataExpiracao != null && dataAtual.before(dataExpiracao)){
            return true;
        } else {
            return false;
        }
    }
}
