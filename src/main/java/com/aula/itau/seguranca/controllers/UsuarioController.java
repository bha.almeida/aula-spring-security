package com.aula.itau.seguranca.controllers;

import com.aula.itau.seguranca.models.Usuario;
import com.aula.itau.seguranca.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario registrarUsuario(@RequestBody @Valid Usuario usuario){
        return usuarioService.salvarUsuario(usuario);
    }

    @GetMapping
    public Iterable<Usuario> listarUsuarios(){
        return usuarioService.lerUsuarios();
    }
}
