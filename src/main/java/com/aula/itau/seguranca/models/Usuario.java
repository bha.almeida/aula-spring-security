package com.aula.itau.seguranca.models;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "usuarios")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "Nome obrigatório")
    @Size(min = 3, message = "Nome deve conter no mínimo 3 letras")
    private String nome;

    @CPF (message = "CPF inválido")
    @NotNull(message = "CPF obrigatório")
    @Column(unique = true)
    private String cpf;

    @Email(message = "Email inválido")
    @NotNull(message = "Email obrigatório")
    @Column(unique = true)
    private String email;

    @NotNull(message = "Senha obrigatória")
    @NotBlank(message = "Senha não pode ser branco")
    private String senha;

    public Usuario() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
